# Lunch Bot

This repo contains two parts: a Rails app and a Slack bot.

## Rails app

The app allows users to add and rate restaurants that will be used by the bot.
Users can also add events and select a restaurant that was used for that event.
Keeping this historical information can be useful for the bot, as it will select
restaurants that are not similar to recent picks. You can read more about this
below under the "Lunch bot" section of this document.

Users can also be manually set as admins. Admins have all normal user abilities
as well as the options to remove restaurants.

Our instance of the app is currently deployed to
https://boiling-caverns-16953.herokuapp.com/. Log in is done via Google OAuth
and currently only supports `@scimedsolutions.com` emails. To enable OAuth, I
followed this guide:

https://www.yuxifan.com/2018/07/25/google-oauth-rails/.

It also requires the following environment variables to be set:

* `GOOGLE_CLIENT_ID`
* `GOOGLE_CLIENT_SECRET`

NOTE: Only `http://localhost:3000` is configured for Google OAuth locally. If
you need to use HTTPS or a different port, you will have to configure that
within the Google Developer console. To do so, log in to the Developer Console,
select the "Lunch Bot" application, and go to Credentials > LunchBot OAuth. From
there, add a new Authorized redirect URI.

To deploy the app, log in via Heroku's CLI using the following command:

        heroku git:remote -a boiling-caverns-16953

Then, all you have to do is run the following:

        git push heroku <branch>

If you need to run a new migration, run the following:

        heroku run rake db:migrate

To add or view environment variables, you can use the following commands:

        # List all environment variables
        heroku config

        # Retrieve the value of FOO
        heroku config:get FOO

        # Set the value of FOO
        heroku config:set FOO=bar

        # Unset the FOO variable
        heroku config:unset FOO

You can also access the Heroku web console by logging in at heroku.com.

## Slack bot

### Using the bot

The Slack Bot is meant to make deciding on a location for lunch easier. To this
aim, it responds to the following commands:

* `help`
  * Prints out information on how to use the bot. You can just say `help` to get
    a list of commands, or you can ask for help about a specific command like
    `help choose`.
* `choose`
  * Sets the chosen restaurant for the next upcoming event.
    Example: `choose Subway`
* `new wtl`
  * Creates a new event called "Weekly Training Lunch" set for the next Monday
    at 12:30 PM and returns three restaurants chosen via the same method as the
    `random` command. Please follow this command with the `choose` command once
    a restaurant has been chosen.
* `random`
    * Returns three semi-random restaurants from the database. If a restaurant
      was chosen for any event that occurred in the past month, it will be
      excluded from the options. Restaurants with the same `food_type` as any
      events from the past week will also be excluded. The randomization is also
      weighted by the ratings provided by the users of the web app. If a
      restaurant has a total of ten stars across all user ratings, it will be
      five times more likely to be chosen than a restaurant that has a total of
      two stars. To make sure that any unrated restaurants have a chance to be
      chosen, any user/restaurant combination that does not have a rating will
      be treated as having 3/5 stars during this process.
* `tiebreaker`
    * Handles breaking ties in the case that multiple restaurants in a given WTL
      get the same number of votes. The restaurant names should be split by
      ` vs ` (case and the use of `.` between the letters are ignored). Missing
      restaurants will be reported. The tie is broken using average rating, and
      if that metric is also tied, a random winner is chosen. As many
      restaurants as desired can be compared (not just 2) as long as each is
      separated by ` vs `.
      Example: `tiebreaker Toast vs Ninth Street vs Luna`

You can trigger the bot by either tagging it or saying its name followed by a
command in any channel that the bot has been invited to. For example:

        @Lunch Bot help
        lunch-bot choose Guasaca

You can also directly message the bot with any commands.

NOTE: as long as the bot is deployed on Heroku, it will be spun down by default
if no one interacts with the web app for a period of time. If the bot fails to
respond, visiting the web app will get it spun back up, and the bot will start
to respond again.

### Enabling the bot

To enable the Slack Bot, the following documentation can be referenced:

* https://github.com/slack-ruby/slack-ruby-bot/tree/v0.12.0
* https://github.com/slack-ruby/slack-ruby-bot/blob/v0.12.0/DEPLOYMENT.md

Deploying the Slack Bot requires the following environment variable:

* `SLACK_API_TOKEN`
