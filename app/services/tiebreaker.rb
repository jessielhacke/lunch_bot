# This class handles breaking ties in the case that multiple restaurants in
# a given WTL get the same number of votes. The restaurant names should be split
# by ` vs ` (case and the use of `.` between the letters are ignored). Missing
# restaurants will be reported. The tie is broken using average rating, and if
# that metric is also tied, a random winner is chosen. As many restaurants as
# desired can be compared (not just 2) as long as each is separated by ` vs `.
class Tiebreaker
  def initialize(user_input)
    @user_input = user_input
    @message = ''
  end

  def tiebreaker_message
    restaurants = find_restaurants
    return @message if @message.present?

    break_tie(restaurants)
    @message
  end

  private def find_restaurants
    missing_names = []

    restaurants = restaurant_names.map do |name|
      restaurant = Restaurant.find_by(name: name)
      missing_names << name unless restaurant.present?

      restaurant
    end

    restaurants_not_found_message(missing_names) if missing_names.any?
    restaurants
  end

  # Split the input into 2 restaurant names separated by ` vs `, ignoring case
  # and the inclusion/exclusion of `.` between the letters
  private def restaurant_names
    @restaurant_names ||= @user_input.split(/\sv[.]?s[.]?\s/i).map(&:strip)
  end

  private def break_tie(restaurants)
    winning_restaurants = highest_rated_restaurants(restaurants)

    if winning_restaurants.one?
      return winning_restaurant_message(winning_restaurants.first)
    end

    random_winner = winning_restaurants.sample
    random_winner_message(random_winner)
  end

  private def highest_rated_restaurants(restaurants)
    max_average_rating = restaurants.map(&:average_rating).compact.max
    restaurants.select do |restaurant|
      restaurant.average_rating == max_average_rating
    end
  end

  private def restaurants_not_found_message(missing_names)
    @message = 'No restaurants with the following names were found: '\
      "#{missing_names.join(', ')}"
  end

  private def winning_restaurant_message(restaurant)
    @message = "The winner is #{restaurant.name} with an average rating of "\
      "#{restaurant.average_rating}"
  end

  private def random_winner_message(random_winner)
    @message = 'Oh no, another tie! :scream: Multiple restaurants had an '\
    "average rating of #{random_winner.average_rating}. The randomly "\
    "selected winner is #{random_winner.name}!"
  end
end
