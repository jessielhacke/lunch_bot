# This class implements a very simple process for weighting a collection where
# each element of the collection will appear in the final collection a number of
# times based on a provided block. For example, if a restaurant has a five-star
# rating and a two-star rating, it would appear in the final weighted collection
# seven times in total (assuming the block passed to `weight_options` returned
# the sum of the ratings for that restaurant).
class OptionWeighter
  def initialize(options)
    @unweighted_options = options
  end

  # Takes a block that should return an integer value for each element in
  # @unweighted_options.
  def weight_options
    @unweighted_options.reduce([]) do |options, option|
      weight = yield(option)
      options + ([option] * weight)
    end
  end
end
