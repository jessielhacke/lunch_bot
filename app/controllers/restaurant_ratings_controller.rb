class RestaurantRatingsController < ApplicationController
  def create
    rating = RestaurantRating.find_or_initialize_by(
      user: current_user,
      restaurant_id: params[:restaurant_id]
    )
    if rating.update(stars: params[:stars])
      head :ok
    else
      head :unprocessable_entity
    end
  end
end
