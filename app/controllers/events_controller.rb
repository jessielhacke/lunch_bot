class EventsController < ApplicationController
  before_action :ensure_admin, only: :destroy
  before_action :set_event, only: %i[show edit update destroy]

  def search
    index
    render :index
  end

  def index
    params[:q] ||= { s: 'occurs_on desc' }
    @search = Event.ransack(params[:q])
    @events = @search.result.page(params[:page])
  end

  def show
  end

  def new
    @event = Event.new
  end

  def create
    @event = Event.new(event_params)

    if @event.save
      redirect_to @event, success: 'Event was successfully created.'
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @event.update(event_params)
      redirect_to @event, success: 'Event was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    if @event.destroy
      redirect_to events_url, success: 'Event was successfully destroyed.'
    else
      redirect_to events_url, warning: @event.errors.to_sentence
    end
  end

  private def set_event
    @event = Event.find(params[:id])
  end

  private def event_params
    params.require(:event).permit(:name, :restaurant_id, :occurs_on)
  end
end
