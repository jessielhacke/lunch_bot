require 'rails_helper'

RSpec.describe Tiebreaker do

  describe '#tiebreaker_message' do
    let(:input_string) { 'Toast vs Ninth Street V.S. Luna' }
    let(:restaurant_1) { build :restaurant, name: 'Toast' }
    let(:restaurant_2) { build :restaurant, name: 'Ninth Street' }
    let(:restaurant_3) { build :restaurant, name: 'Luna' }

    subject { described_class.new(input_string) }

    before do
      allow(Restaurant).to receive(:find_by).with(name: restaurant_1.name) { restaurant_1 }
      allow(Restaurant).to receive(:find_by).with(name: restaurant_2.name) { restaurant_2 }
      allow(Restaurant).to receive(:find_by).with(name: restaurant_3.name) { restaurant_3 }
      allow(restaurant_1).to receive(:average_rating) { 3.5 }
      allow(restaurant_2).to receive(:average_rating) { 3.8 }
      allow(restaurant_3).to receive(:average_rating) { 3.2 }
    end

    context 'when all the restaurants can be found' do
      context 'when multiple restaurants share the same winning average rating' do
        before do
          allow(restaurant_1).to receive(:average_rating) { 3.8 }
        end

        it 'returns a message picking a random winner' do
          expected_message = 'Oh no, another tie! :scream: Multiple restaurants had an '\
            "average rating of #{restaurant_1.average_rating}. The randomly selected winner is "
          expect(subject.tiebreaker_message).to include expected_message
        end
      end

      context 'when one restaurant has the highest average rating' do
        it 'returns a message indicating the winner' do
          expected_message = "The winner is #{restaurant_2.name} with an "\
            "average rating of #{restaurant_2.average_rating}"
          expect(subject.tiebreaker_message).to eql expected_message
        end
      end
    end

    context 'when a restaurant cannot be found' do
      before do
        allow(Restaurant).to receive(:find_by).with(name: restaurant_2.name) { nil }
        allow(Restaurant).to receive(:find_by).with(name: restaurant_3.name) { nil }
      end

      it 'returns a message about the invalid name(s)' do
        expected_message = 'No restaurants with the following names were found: '\
          'Ninth Street, Luna'
        expect(subject.tiebreaker_message).to eql expected_message
      end
    end
  end
end
