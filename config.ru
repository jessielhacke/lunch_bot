# frozen_string_literal: true

# This file is used by Rack-based servers to start the application.

require_relative 'config/environment'
require ::File.expand_path('../bot/weekly_training_lunch_bot', __FILE__)

unless Rails.env.development?
  Thread.abort_on_exception = true
  Thread.new { WeeklyTrainingLunchBot.run }
end

run Rails.application
