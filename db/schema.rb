# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_19_143348) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "diet_incompatibilities", force: :cascade do |t|
    t.bigint "restaurant_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["restaurant_id"], name: "index_diet_incompatibilities_on_restaurant_id"
    t.index ["user_id"], name: "index_diet_incompatibilities_on_user_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "name", null: false
    t.date "occurs_on", null: false
    t.bigint "restaurant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["occurs_on"], name: "index_events_on_occurs_on"
    t.index ["restaurant_id"], name: "index_events_on_restaurant_id"
  end

  create_table "restaurant_ratings", force: :cascade do |t|
    t.integer "stars", default: 3, null: false
    t.bigint "restaurant_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["restaurant_id", "user_id"], name: "index_restaurant_ratings_on_restaurant_id_and_user_id", unique: true
    t.index ["restaurant_id"], name: "index_restaurant_ratings_on_restaurant_id"
    t.index ["user_id"], name: "index_restaurant_ratings_on_user_id"
  end

  create_table "restaurants", force: :cascade do |t|
    t.string "name", null: false
    t.string "menu_url", null: false
    t.string "food_type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "delivery_method"
  end

  create_table "users", force: :cascade do |t|
    t.boolean "admin", default: false, null: false
    t.string "email", null: false
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end


  create_view "user_ratings", sql_definition: <<-SQL
      SELECT restaurant_ratings.id,
      COALESCE(restaurant_ratings.stars, 3) AS rating,
      restaurants.id AS restaurant_id,
      users.id AS user_id
     FROM ((users
       CROSS JOIN restaurants)
       LEFT JOIN restaurant_ratings ON (((restaurant_ratings.user_id = users.id) AND (restaurant_ratings.restaurant_id = restaurants.id))));
  SQL
end
